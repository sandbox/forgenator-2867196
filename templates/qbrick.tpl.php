<?php

// get necessary variables
$accountid = variable_get('qbrick_accountid');
$configurationid = variable_get('qbrick_configurationid');
$mediaid = $variables['element']['value'];

// if we don't have configurationid then use default
if(!$configurationid) {
    $configurationid = "default";
}

// add external js only when we run the player

drupal_add_js('//play2.qbrick.com/framework/GoBrain.min.js', 'external');

?>

<div id="divPageContainer">
    <div id="divPlayerContainer"></div>
</div>
<script type="text/javascript">
    var embedSettings = {
        config: '//video.qbrick.com/play2/api/v1/accounts/<?php print $accountid; ?>/configurations/<?php print $configurationid; ?>',
        data: '//video.qbrick.com/api/v1/public/accounts/<?php print $accountid; ?>/medias/<?php print $mediaid; ?>',
        widgetId: 'player'
    };

    GoBrain.create(document.getElementById('divPlayerContainer'), embedSettings);
</script>
